import sys, os, re
from collections import defaultdict
from math import ceil

class Game:
	def __init__(self,filename):
		self.line_by_line = []
		self.parse_raw_data(filename)
		self.pos = 0
		self.player_bats = defaultdict(int)
		#self.player_bats = {}
		self.player_hits = defaultdict(int)
		self.player_runs = defaultdict(float)
		self.player_avg_fullname = {}
		self.parse_player()

	def parse_raw_data(self, filename):
		f = open(filename)
		for line in f:
			if line != "\n" and line.find("===") and line.find("#"):
				self.line_by_line.append(line.rstrip())
		f.close()

	def parse_player(self):
		for s in self.line_by_line:
			first_name, last_name = get_name(s)
			first_name = first_name.rstrip()
			last_name = last_name.rstrip()
			bats, hits, runs = get_hits_and_runs(s)
			self.player_bats[first_name, last_name] += int(bats)
			self.player_hits[first_name, last_name] += int(hits)
			self.player_runs[first_name, last_name] += int(runs)
			
		for i in self.player_bats:
			first_name, last_name = i[0], i[1]
			hits = float(self.player_hits[first_name, last_name])
			bats = float(self.player_bats[first_name, last_name])
			full_name = first_name + " " + last_name
			self.player_avg_fullname[full_name] = round(hits / bats,3)
			self.player_avg_fullname[full_name] = "{0:.3f}".format(self.player_avg_fullname[full_name])
		sorted_avg = sorted(self.player_avg_fullname.items(), key=lambda x: x[1],reverse=True)
		for name, avg in sorted_avg:
			print str(name + ":  " + str(avg))

			
def get_name(line):
	#my_str = "Charlie Gelbert batted 2 times with 1 hits and 1 runs"
	return re.findall(r'([A-Z]\w+)',line)

def get_hits_and_runs(line):
	return re.findall(r'\d+?',line)

if __name__ == "__main__":
	if len(sys.argv) != 2:
	    sys.exit("Argument Error: the program needs the input file!\nUsage: python exercise2.py cardinals-1930.txt")
	filename = sys.argv[1]
	if not os.path.exists(filename):
		sys.exit("Error: File '%s' not found" % sys.argv[1])

	
	g = Game(filename)
