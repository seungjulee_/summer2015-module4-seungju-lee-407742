import sys, os, re

def print_problem(result):
	for i in result:
		print i

def test_prob1_regex(test):
	prob1_result = re.findall(r'\bhello world\b',test,re.IGNORECASE)
	return prob1_result

def test_prob2_regex(test):
	prob2_result = re.findall(r'\b\w+?[aeiou]{3}\w+?\b',test,re.IGNORECASE)
	return prob2_result

def test_prob3_regex(test):
	prob3_result = re.findall(r'\bAA\d{3,4}\b',test,re.IGNORECASE)
	return prob3_result

if __name__ == "__main__":
	print len(sys.argv)
	if len(sys.argv) != 3:
		print "-----------------------------"
		print "USAGE: python exercise1.py 2 prob2.txt"
		print "-----------------------------"
		print "The above command execute problem 2 of the exercise."
		print ""
		sys.exit("Argument Error: the program needs two arguments!")
	prob_num = sys.argv[1]
	filename = sys.argv[2]
	f = open(filename)
	prob_content = f.read()
	f.close()

	if not os.path.exists(filename):
		sys.exit("Error: File '%s' not found" % sys.argv[1])

	print "--------------"
	print "PROBLEM" ,prob_num, "Text"
	print "--------------"
	print prob_content
	print "----------------"
	print "PROBLEM", prob_num ,"Result"
	print "----------------"

	if prob_num == "1":
		print_problem(test_prob1_regex(prob_content))
	elif prob_num == "2":
		print_problem(test_prob2_regex(prob_content))
	elif prob_num == "3":
		print_problem(test_prob3_regex(prob_content))
